import logo from './logo.svg';
import './App.css';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import { Col, Container, Row, FormControl, Button } from 'react-bootstrap';
import { useEffect, useState } from 'react';

function App() {

  const[data,setData]=useState([]);
  const[from,setFrom]=useState('USD');
  const[to,setTo]=useState('CRC');
  const[amount,setAmount]=useState(1);
  const[value,setValue]=useState();
  //const[baseUrl]=useState("http://localhost:16683");
  const[baseUrl]=useState("");

  useEffect(()=>{
    let uri = `${baseUrl}/v1/currencies`
    fetch(uri,{
      method:'GET',
      headers:{
        'content-type':'application/json'
      }
    })
    .then(response => response.json())
    .then(resp=>{
      var res = [];
      for (const [key,value] of Object.entries(resp.response.fiats)) {
        res.push({key:key,name:value.currency_name, code:value.currency_code});
      }
      return res;
    })
    .then(resp=>setData(resp))
  },[])

  const fromHandle=(e)=>{
    setFrom(e);
  }

  const toHandle=(e)=>{
    setTo(e);
  }

  const amountHandle=(e)=>{
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
      setAmount(e.target.value);
    } 
  }

  const converHandle=()=>{
    if(amount > 0){
      console.log("convert from: " + from + " to: " + to + " amount: " + amount);
      let uri = `${baseUrl}/v1/convert/from/${from}/to/${to}/amount/${amount}`;
      fetch(uri,{
        method:'GET',
        headers:{
          'content-type':'application/json'
        }
      })
      .then(response => response.json())
      .then(resp=>{
        setValue(resp.response.value);      
      })
    }
    else{
      alert("Amount must be greater than 0 !")
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>

              <Container>
              <Row><Col><br></br></Col></Row>
                <Row>
                  <Col>
                    <label>From:</label>
                  </Col>
                  <Col>
                    <DropdownButton id="fromDdl" title={from} onSelect={fromHandle}>
                        {
                          data.map((items)=>{
                            return(
                              <Dropdown.Item key={items.key} eventKey={items.code}>{items.code}</Dropdown.Item>
                            );
                          })
                        }
                      </DropdownButton>
                  </Col>
                </Row>
                <Row><Col><br></br></Col></Row>
                <Row>
                  <Col>
                    <label>To:</label>
                  </Col>
                  <Col>
                    <DropdownButton id="toDdl" title={to} onSelect={toHandle}>
                      {
                        data.map((items)=>{
                          return(
                            <Dropdown.Item key={items.key} eventKey={items.code}>{items.code}</Dropdown.Item>
                          );
                        })
                      }
                    </DropdownButton>
                  </Col>
                </Row>
                <Row><Col><br></br></Col></Row>
                <Row>
                  <Col>
                    <label >Amount:</label>
                  </Col>
                  <Col>
                  <FormControl id="amountTxt" value={amount} onChange={amountHandle} aria-describedby="basic-addon1" />
                  </Col>
                </Row>
                <Row><Col><br></br></Col></Row>
                <Row>
                  <Col>
                    <Button id="convertBtn" onClick={converHandle} variant="success">Convert</Button>
                  </Col>
                  <Col>
                  <FormControl id="valueTxt" value={value} aria-describedby="basic-addon1" />
                  </Col>
                </Row>
              </Container>

        </div>
        <br></br><br></br><br></br><br></br><br></br><br></br><br></br>
      </header>
    </div>
  );
}

export default App;
